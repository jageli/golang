package main

import (
	"fmt"
	"log"

	"gitlab.com/jageli/golang/learn1/lib1"

	"github.com/tidwall/gjson"
)

func main() {
	fmt.Println("hello")
	li := lib1.Add(1, 2)
	fmt.Printf("%v\n", li)
	log.Println(li)
	const json = `{"name":{"first":"Janet","last":"Prichard"},"age":47}`
	value := gjson.Get(json, "name.last")
	println(value.String())

}
