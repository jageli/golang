package main

import (
	"fmt"
	"os"
)

func main() {

	args := os.Args //获取用户输入的所有参数
	//args的类型是[]string

	one := args[1] //获取输入的第一个参数
	two := args[2] //获取输入的第二个参数
	fmt.Println(one)
	fmt.Println(two)

}
